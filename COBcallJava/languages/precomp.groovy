import com.ibm.dbb.build.*
import groovy.util.*

println("Pre compiling COBOL program . . .")

def workspace = args[0]
def member = args[1]
def hlq = args[2]
def cpy = args[3]

def memberFile = new CopyToPDS()
memberFile.setDataset("${hlq}.COBOL")
memberFile.setMember("$member")
memberFile.setFile(new File("${workspace}/COBcallJava/cobol/${member}.cbl"))
memberFile.execute()

def folder = new File("${workspace}/COBcallJava/copybook")
def files = folder.listFiles()
def copyFile = new CopyToPDS()
def fileName
if (files) {
    files.each { file ->
        fileName = file.name
        if (fileName.contains('.')) {
        fileName = fileName.substring(0, fileName.lastIndexOf('.'))
        }
        copyFile.setFile(new File("${workspace}/COBcallJava/copybook/${file.name}"))
        copyFile.setDataset("${hlq}.COPY")
        copyFile.setMember("${fileName}")
        copyFile.execute()
    }
} else {
    println "No files found in the folder."
}

def lked = new MVSExec().pgm("DSNHPC").parm("HOST(COBOL),APOST,SOURCE,LEVEL(GLM)")

lked.dd(new DDStatement().name("DBRMLIB").dsn("CBS.ZCON.DBRMLIB(${member})").options("shr"))
lked.dd(new DDStatement().name("SYSLIB").dsn("${hlq}.COPY").options("shr"))
lked.dd(new DDStatement().name("SYSIN").dsn("${hlq}.COBOL(${member})").options("shr"))
lked.dd(new DDStatement().name("TASKLIB").dsn("DSNC10.SDSNLOAD").options("shr"))
lked.dd(new DDStatement().name("SYSCIN").dsn("${hlq}.COBOL(${member})").options("shr"))
lked.dd(new DDStatement().name("SYSUT2").options("cyl space(5,5) unit(vio) blksize(80) lrecl(80) recfm(f,b) new"))
lked.dd(new DDStatement().name("SYSUT1").options("cyl space(5,5) unit(vio) blksize(80) lrecl(80) recfm(f,b) new"))
lked.dd(new DDStatement().name("SYSPRINT").options("cyl space(5,5) unit(vio) blksize(133) lrecl(133) recfm(f,b) new"))
lked.copy(new CopyToHFS().ddName("SYSPRINT").file(new File("${workspace}/${member}.precomplog")))

rc = lked.execute()

if (rc > 4) {
    String command = "cat ${workspace}/${member}.precomplog "

    ProcessBuilder builder = new ProcessBuilder("sh", "-c", command)
    builder.redirectErrorStream(true)

    Process process = builder.start()
    process.waitFor()
    if (process.exitValue() == 0) {
    String output = process.getInputStream().getText()
    println "$output"
    }

    throw new Exception("Return code is greater than 4! RC=$rc")
} else {
    println("Precompile successful! RC=$rc")
}
