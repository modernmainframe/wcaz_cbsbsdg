import com.ibm.dbb.build.*
import groovy.util.*

println("Binding COBOL program . . .")

def hlq = args[0]
def member = args[1]
def linkLog = args[2]
def owner = args[3]

def bindinstream = """DSN SYSTEM(DBCG)                                      
        BIND PLAN(COREBK)                                    
        MEMBER(${member})                            
        LIBRARY ('CBS.ZCON.DBRMLIB')                      
        ACTION (REPLACE), VALIDATE(RUN), ISOLATION (CS) , FLAG (I), 
ACQUIRE (USE) , RELEASE (COMMIT)  , EXPLAIN (NO), RETAIN, 
OWNER(${owner}), QUALIFIER(${owner}), ENCODING(EBCDIC) 
   END """             


def lked = new MVSExec().pgm("IKJEFT01")

lked.dd(new DDStatement().name("TASKLIB").dsn("DSNC10.SDSNLOAD").options("shr"))

lked.dd(new DDStatement().name("SYSTSIN").instreamData(bindinstream))

lked.dd(new DDStatement().name("SYSTSPRT").options("cyl space(5,5) unit(vio) blksize(133) lrecl(133) recfm(f,b) new"))
lked.copy(new CopyToHFS().ddName("SYSTSPRT").file(new File("${linkLog}/${member}.bindlog")))

rc = lked.execute()

if (rc > 4) {
    String command = "cat ${linkLog}/${member}.bindlog "

    ProcessBuilder builder = new ProcessBuilder("sh", "-c", command)
    builder.redirectErrorStream(true)

    Process process = builder.start()
    process.waitFor()
    if (process.exitValue() == 0) {
    String output = process.getInputStream().getText()
    println "$output"
    }

    throw new Exception("Return code is greater than 4! RC=$rc")
} else {
    println("Binding successful! RC=$rc")
}
